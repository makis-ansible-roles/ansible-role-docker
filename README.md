Docker
=========

This Ansible role installs Docker by removing old Docker installs, adding the Docker APT key and repository, installing required packages, adding the current user to the Docker group, starting the Docker service, and adding a cron job to prune Docker if needed.

Requirements
------------

This role requires Ansible 2.1 or higher and Debian 10 or higher.

Role Variables
--------------

- docker_gpg_url: https://download.docker.com/linux/ubuntu
- docker_gpg_path: /etc/apt/trusted.gpg.d/docker.gpg
- docker_prune_job: false

Dependencies
------------

This role has no dependencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - ansible-role-docker

License
-------

MIT.

Author Information
------------------

This role was created by Pavel Makis
